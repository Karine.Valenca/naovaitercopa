package model;

public class Solucao {
	private Governo autor;
	private String titulo;
	private Comentario comentario;
	
	public Solucao(Governo autor, String titulo){
		this.autor = autor;
		this.titulo = titulo;
	}
	
	public Governo getAutor() {
		return autor;
	}
	public void setAutor(Governo autor) {
		this.autor = autor;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public Comentario getComentario() {
		return comentario;
	}
	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}
	
	public void receberRelevancia(){
		
	}
	
}
