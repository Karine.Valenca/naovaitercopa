package model;

public class Comentario {
	private String conteudo;
	private Usuario autor;
	
	public Comentario(){
		
	}
	
	public Comentario(String conteudo, Usuario autor){
		this.conteudo = conteudo;
		this.autor = autor;		
	}
	
	public String getConteudo() {
		return conteudo;
	}
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	public Usuario getAutor() {
		return autor;
	}
	public void setAutor(Usuario autor) {
		this.autor = autor;
	}
	
	
}
