package model;

public class Governo extends Usuario{

	private String idGoverno;
	
	public Governo(){
		
	}
	
	public Governo(String nomeUsuario, String senha, String idGoverno){
		super(nomeUsuario, senha);
		this.idGoverno = idGoverno;
	}
	
	public String getIdGoverno() {
		return idGoverno;
	}

	public void setIdGoverno(String idGoverno) {
		this.idGoverno = idGoverno;
	}
	
	public void apresentarSolucao(){
		
	}
}
