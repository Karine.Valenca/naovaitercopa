package model;


public class Reivindicacao {
	private String titulo;
	private String conteudo;
	private Populacao autor;
	private Comentario comentario;
	
	public Reivindicacao(){
		
	}
	
	public Reivindicacao(String titulo, String conteudo, Populacao autor){
		this.titulo = titulo;
		this.conteudo = conteudo;
		this.autor = autor;
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getConteudo() {
		return conteudo;
	}
	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}
	public Populacao getAutor() {
		return autor;
	}
	public void setAutor(Populacao autor) {
		this.autor = autor;
	}
	public Comentario getComentario() {
		return comentario;
	}
	public void setComentario(Comentario comentario) {
		this.comentario = comentario;
	}
	
	public void receberRelevancia(){
		
	}
	
	public void receberSolucaco(){
		
	}
}
