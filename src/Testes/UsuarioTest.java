package Testes;

import static org.junit.Assert.*;

import model.Usuario;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class UsuarioTest {
	Usuario umUsuario;

	@Before
	public void setUp() throws Exception {
		umUsuario = new Usuario();
	}

	@Test
	public void testSetGetNomeUsuario() {
		umUsuario.setNomeUsuario("Usuario");
		assertEquals("Usuario", umUsuario.getNomeUsuario());
	}

	@Test
	public void testSetGetSenha() {
		umUsuario.setSenha("Senha");
		assertEquals("Senha", umUsuario.getSenha());
	}

	@Test
	public void testSetGetEmail() {
		umUsuario.setEmail("Email");
		assertEquals("Email", umUsuario.getEmail());
	}

	@Test
	public void testSetGetNome() {
		umUsuario.setNome("Nome");
		assertEquals("Nome", umUsuario.getNome());
	}

	@Test
	public void testSetGetPais() {
		umUsuario.setPais("Pais");
		assertEquals("Pais", umUsuario.getPais());
	}

	@Test
	public void testSetGetEstado() {
		umUsuario.setEstado("Estado");
		assertEquals("Estado", umUsuario.getEstado());
	}

	@Test
	public void testSetGetCidade() {
		umUsuario.setCidade("Cidade");
		assertEquals("Cidade", umUsuario.getCidade());
	}

	@Test
	public void testSetGetAtividadeProfissional() {
		umUsuario.setAtividadeProfissional("Atividade");
		assertEquals("Atividade", umUsuario.getAtividadeProfissional());
	}

	@Test
	public void testSetGetOrganizacao() {
		umUsuario.setOrganizacao("Organizacao");
		assertEquals("Organizacao", umUsuario.getOrganizacao());
	}

}
