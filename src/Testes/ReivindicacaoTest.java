package Testes;

import static org.junit.Assert.*;

import model.Populacao;
import model.Reivindicacao;

import org.junit.Before;
import org.junit.Test;

public class ReivindicacaoTest {
	Reivindicacao umaReivindicacao;
	Populacao umaPopulacao;
	
	@Before
	public void setUp() throws Exception {
		umaReivindicacao = new Reivindicacao();
		umaPopulacao = new Populacao();
	}

	@Test
	public void testSetGetTitulo() {
		umaReivindicacao.setTitulo("Titulo");
		assertEquals("Titulo", umaReivindicacao.getTitulo());
	}

	@Test
	public void testSetGetConteudo() {
		umaReivindicacao.setConteudo("Conteudo");
		assertEquals("Conteudo", umaReivindicacao.getConteudo());
	}

	@Test
	public void testSetGetAutor() {
		umaReivindicacao.setAutor("Autor");
		assertEquals("Autor", umaReivindicacao.getAutor());
	}

	@Test
	public void testSetGetComentario() {
		umaReivindicacao.setComentario("Comentario");
		assertEquals("Comentario", umaReivindicacao.getComentario());
	}

}
