package Testes;

import static org.junit.Assert.*;

import model.Administrador;

import org.junit.Before;
import org.junit.Test;

public class AdministradorTest {
	Administrador umAdministrador;
	
	@Before
	public void setUp() throws Exception {
		umAdministrador = new Administrador();
	}

	@Test
	public void testSetGetNomeUsuario() {
		umAdministrador.setNomeUsuario("NomeUsuario");
		assertEquals("NomeUsuario", umAdministrador.getNomeUsuario());
	}

	@Test
	public void testSetGetSenha() {
		umAdministrador.setSenha("Senha");
		assertEquals("Senha", umAdministrador.getSenha());
	}

}
