package Testes;

import static org.junit.Assert.*;

import model.Comentario;

import org.junit.Before;
import org.junit.Test;

public class ComentarioTest {
	Comentario umComentario;
	@Before
	public void setUp() throws Exception {
		
		umComentario = new Comentario();
	}

	@Test
	public void testSetGetConteudo() {
		umComentario.setConteudo("Conteudo");
		assertEquals("Conteudo", umComentario.getConteudo());
	}

	@Test
	public void testGetAutor() {
		umComentario.setAutor("Autor");
		assertEquals("Autor", umComentario.getAutor());
	}

}
