package Testes;

import static org.junit.Assert.*;

import model.Governo;

import org.junit.Before;
import org.junit.Test;

public class GovernoTest {
	Governo umGoverno;
	
	@Before
	public void setUp() throws Exception {
		umGoverno = new Governo();
	}

	@Test
	public void testSetGetIdGoverno() {
		umGoverno.setIdGoverno("ID");
		assertEquals("ID", umGoverno.getIdGoverno());
	}

}
